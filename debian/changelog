ocamlify (0.0.2-11) unstable; urgency=medium

  * Team upload
  * Fix build with OCaml 5.x

 -- Stéphane Glondu <glondu@debian.org>  Tue, 18 Jun 2024 09:01:32 +0200

ocamlify (0.0.2-10) unstable; urgency=medium

  * Team upload
  * Recompile with OCaml 4.14.1
  * Make ocamlify an arch:any package
  * Bump Standards-Version to 4.6.2

 -- Stéphane Glondu <glondu@debian.org>  Thu, 21 Sep 2023 07:05:58 +0200

ocamlify (0.0.2-9) unstable; urgency=medium

  * Team upload
  * Recompile with OCaml 4.13.1
  * Bump Standards-Version to 4.6.0
  * Bump debian/watch version to 4

 -- Stéphane Glondu <glondu@debian.org>  Thu, 20 Jan 2022 16:32:02 +0100

ocamlify (0.0.2-8) unstable; urgency=medium

  * Team upload
  * Recompile with OCaml 4.11.1
  * Bump debhelper compat level to 13
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Wed, 14 Oct 2020 06:07:17 +0200

ocamlify (0.0.2-7) unstable; urgency=medium

  * Team upload
  * Recompile with OCaml 4.08.1

 -- Stéphane Glondu <glondu@debian.org>  Fri, 08 Nov 2019 16:38:47 +0100

ocamlify (0.0.2-6) unstable; urgency=medium

  * Team upload
  * Fix compilation with -safe-string
  * Update Vcs-*
  * Bump debhelper compat level to 12
  * Bump Standards-Version to 4.4.0

 -- Stéphane Glondu <glondu@debian.org>  Mon, 19 Aug 2019 11:31:54 +0200

ocamlify (0.0.2-5) unstable; urgency=medium

  * debhelper compat level 10
    - d/rules: fix order of arguments in invocation of dh
  * d/copyright: machine-readable format 1.0
  * Standards-version 4.1.0(no change)
  * add myself to Uploaders
  * patch deprecated_ocaml_functions: replace some ocaml functions

 -- Ralf Treinen <treinen@debian.org>  Mon, 25 Sep 2017 20:18:56 +0200

ocamlify (0.0.2-4) unstable; urgency=medium

  * Team upload
  * Add ocamlbuild to Build-Depends

 -- Stéphane Glondu <glondu@debian.org>  Fri, 14 Jul 2017 15:29:05 +0200

ocamlify (0.0.2-3) unstable; urgency=low

  [ Sylvain Le Gall ]
  * Remove Sylvain Le Gall from Uploaders.

  [ Mehdi Dogguy ]
  * Rebuild with OCaml 4.02.3

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 10 Oct 2015 19:15:21 +0200

ocamlify (0.0.2-2) unstable; urgency=low

  * Team upload
  * Upload to unstable

 -- Stéphane Glondu <glondu@debian.org>  Tue, 03 Dec 2013 08:22:29 +0100

ocamlify (0.0.2-1) experimental; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Compile with OCaml >= 4
  * Update Vcs-*

  [ Sylvain Le Gall ]
  * New upstream version.

 -- Stéphane Glondu <glondu@debian.org>  Wed, 24 Jul 2013 21:34:36 +0200

ocamlify (0.0.1-3) unstable; urgency=low

  * Team upload
  * Recompile with OCaml 3.12.1 (no changes)

 -- Stéphane Glondu <glondu@debian.org>  Wed, 02 Nov 2011 07:23:22 +0100

ocamlify (0.0.1-2) unstable; urgency=low

  * Team upload
  * Rebuild with OCaml 3.12.0 (no changes)
  * debian/control: add Homepage field
  * Add debian/watch

 -- Stéphane Glondu <glondu@debian.org>  Sun, 17 Apr 2011 21:00:49 +0200

ocamlify (0.0.1-1) unstable; urgency=low

  * Initial release. (Closes: #602913)

 -- Sylvain Le Gall <gildor@debian.org>  Wed, 17 Nov 2010 22:09:41 +0100
